package com.dashboard.controller;

import com.dashboard.model.ProjectManagerResponse;
import com.dashboard.model.ProjectResponse;
import com.dashboard.service.ProjectManagerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/manager")
@Api(tags = "Project Manager Controller", description = "Retrieve project manager details")
public class ProjectManagerController {

    @Autowired
    ProjectManagerService service;

    private HttpServletRequest request;

    @GetMapping("/getAllManagerDetails")
    @ApiOperation(value = "Gets Project Manager Detail by project Id", notes = "Gets Project Manager Detail by project Id")
    @ApiResponses(
            value = {@ApiResponse(code = 200, message = "OK", response = ProjectManagerResponse.class, responseContainer = "List")})
    public ResponseEntity<List<ProjectManagerResponse>> getAllProjectManagerDetails(@RequestHeader String Authorization) throws Exception {
        List<ProjectManagerResponse> response = service.getAllProjectManagerDetails(Authorization);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/{projectId}")
    @ApiOperation(value = "Gets Project Manager Detail by project Id", notes = "Gets Project Manager Detail by project Id")
    @ApiResponses(
            value = {@ApiResponse(code = 200, message = "OK", response = ProjectManagerResponse.class),
                    @ApiResponse(code = 404, message = "Data does not exist", response = ResponseStatusException.class)})
    public ResponseEntity<ProjectManagerResponse> getProjectManagerDetails(@PathVariable(required = true) int projectId, @RequestHeader String Authorization) throws Exception {
        ProjectManagerResponse response = service.getProjectManagerDetailsById(projectId, Authorization);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
