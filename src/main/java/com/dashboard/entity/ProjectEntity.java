package com.dashboard.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Entity
@Data
public class ProjectEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
    private Long projectId;
	
	@Column(name = "project_name", length = 32, nullable = false)
	private String projectName;
	
	@Column(name = "description")
	private String description;
}
