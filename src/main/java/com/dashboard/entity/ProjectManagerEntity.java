package com.dashboard.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class ProjectManagerEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

   @Column
    private String project;


    @Column
    private int project_id;


    @Column
    private String Onsite_PM;

    @Column
    private String Offshore_PM;

    @Column
    private String delivery_lead;

    @Column
    private String portfolio_lead;
}
