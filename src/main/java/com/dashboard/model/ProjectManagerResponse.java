package com.dashboard.model;

import lombok.Data;

import javax.persistence.Column;

@Data
public class ProjectManagerResponse {
    private Long id;
    private String project;
    private int project_id;
    private UserDetails onsitePMDetails;
    private UserDetails deliveryLead;
    private UserDetails offShorePMDetails;
    private UserDetails portfolioLeadDetails;
}
