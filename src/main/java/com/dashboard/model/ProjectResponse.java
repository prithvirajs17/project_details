package com.dashboard.model;

import lombok.Data;

import javax.persistence.Column;

@Data
public class ProjectResponse {
    private Long projectId;
    private String projectName;
    private String description;
}
