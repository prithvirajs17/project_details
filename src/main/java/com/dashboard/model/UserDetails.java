package com.dashboard.model;

import lombok.Data;

@Data
public class UserDetails {
    private Long userId;
    private String userName;
    private String email;
    private String firstName;
    private String lastName;
    private String status;
    private String createdBy;
    private String modifiedBy;
    private String createdDate;
    private String modifiedDate;
}
