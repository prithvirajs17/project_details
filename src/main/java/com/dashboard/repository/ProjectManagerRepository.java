package com.dashboard.repository;

import com.dashboard.entity.ProjectManagerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ProjectManagerRepository extends JpaRepository<ProjectManagerEntity, Long> {
    @Query("select pr from ProjectManagerEntity pr where pr.project_id=:project_id")
    ProjectManagerEntity findByProjectIdId(@Param("project_id") int project_id);


}
