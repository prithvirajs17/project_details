package com.dashboard.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dashboard.entity.ProjectEntity;

public interface ProjectRepository extends JpaRepository<ProjectEntity, Long>{

}
