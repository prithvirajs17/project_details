package com.dashboard.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.dashboard.model.ProjectResponse;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.dashboard.entity.ProjectEntity;
import com.dashboard.repository.ProjectRepository;
import org.springframework.web.server.ResponseStatusException;

@Service
public class ProjectService {

	@Autowired
	ProjectRepository repository;
	
	public void saveProjectDetails(ProjectResponse request) {
		ProjectEntity entity= new ProjectEntity();
		BeanUtils.copyProperties(request,entity);
		repository.save(entity);
	}
	
	public ProjectResponse getProjectById(Long projectId) throws Exception {
		Optional<ProjectEntity> projectDetails = repository.findById(projectId);
		if(projectDetails.isPresent()) {
			ProjectResponse response= new ProjectResponse();
			BeanUtils.copyProperties(projectDetails.get(),response);
			return response;
		}else {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Data does not exist with the projectId " + projectId);
		}
	}

	public List<ProjectResponse> getAllProjects() {
		List<ProjectEntity> allProjects = repository.findAll();
		return mapProjectResponse(allProjects);

	}

	private List<ProjectResponse> mapProjectResponse(List<ProjectEntity> allProjects) {
		List<ProjectResponse> response = new ArrayList<>();
		for(ProjectEntity entity:allProjects){
			ProjectResponse projectResponse= new ProjectResponse();
			BeanUtils.copyProperties(entity,projectResponse);
			response.add(projectResponse);
		}
		return response;
	}
}
