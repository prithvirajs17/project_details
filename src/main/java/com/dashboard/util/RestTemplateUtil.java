package com.dashboard.util;

import com.dashboard.model.UserResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.ValidationException;
import java.util.Arrays;
import java.util.List;

@Component
public class RestTemplateUtil {

    @Autowired
    RestTemplate restTemplate;

    @Value("${get.user.all}")
    String userDetails;

    public List<UserResponse> getUserDetails(String token){
        String errorMsg1 = "Error accessing the User API end-point. (" + userDetails + ")";
        String errorMsg2 = "Unavailable User API (" + userDetails + ")";
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.set("Autorization",token);
        HttpEntity<String> entity = new HttpEntity<String>(headers);

        ResponseEntity<List<UserResponse>> response = restTemplate.exchange(userDetails,
                HttpMethod.GET,entity, new ParameterizedTypeReference<List<UserResponse>>() {});
   /*     if (response.getStatus() == HttpStatus.BAD_REQUEST.value()) {
            CommonUtils.constructValidationException(ErrorCodes.BAD_REQUEST.getCode(), "UnitId(s)",
                    "No data is available");
        }
        if ( !( response.getStatusCode() == HttpStatus.OK.value()
                || response.getStatus() == HttpStatus.CREATED.value() )) {

            throw new ResponseStatusException(response.getStatusCode(), errorMsg1);
        }*/

               return response.getBody();

    }
}
